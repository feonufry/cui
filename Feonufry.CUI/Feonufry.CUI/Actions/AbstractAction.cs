﻿
using System;

namespace Feonufry.CUI.Actions
{
	public abstract class AbstractAction : IAction
	{
		#region Methods

		public void Perform(ActionExecutionContext context)
		{
			try
			{
				InnerPerform(context);
			}
			catch (Exception e)
			{
				context.Out.WriteLine(ConsoleColor.Red, e.Message);
				if(!context.ExceptionsAreSkipped)
				{
					throw;
				}
			}
		}

		#endregion Methods

		#region Helpers

		protected abstract void InnerPerform(ActionExecutionContext context);

		#endregion Helpers
	}
}
