﻿using System;

namespace Feonufry.CUI.Actions
{
	public interface IActionFactory
	{
		IAction Create(Type type);
		void Release(IAction action);
	}
}