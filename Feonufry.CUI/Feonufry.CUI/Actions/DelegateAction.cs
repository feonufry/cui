﻿using System;

namespace Feonufry.CUI.Actions
{
	public class DelegateAction : AbstractAction
	{
		private readonly Action<ActionExecutionContext> _actionDelegated;

		public DelegateAction(Action<ActionExecutionContext> action)
		{
			_actionDelegated = action;
		}

		protected override void InnerPerform(ActionExecutionContext context)
		{
			_actionDelegated(context);
		}
	}
}