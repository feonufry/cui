﻿using System;

namespace Feonufry.CUI.Actions
{
	public class DefaultActionFactory : IActionFactory
	{
		public IAction Create(Type type)
		{
			return Activator.CreateInstance(type) as IAction;
		}

		public void Release(IAction action)
		{
			var disposable = action as IDisposable;
			if (disposable != null)
			{
				disposable.Dispose();
			}
		}
	}
}