﻿namespace Feonufry.CUI.Actions
{
	public class AlwaysAvailable : IAvailability
	{
		public bool Available 
		{
			get { return true; }
		}
	}
}