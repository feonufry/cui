﻿using System;

namespace Feonufry.CUI.Actions
{
	public class DelegateAvailability : IAvailability
	{
		private readonly Func<bool> _delegate;

		public DelegateAvailability(Func<bool> @delegate)
		{
			if (@delegate == null) throw new ArgumentNullException("delegate");
			_delegate = @delegate;
		}

		public bool Available
		{
			get { return _delegate(); }
		}
	}
}