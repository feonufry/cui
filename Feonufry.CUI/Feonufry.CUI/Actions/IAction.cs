﻿
namespace Feonufry.CUI.Actions
{
	public interface IAction
	{
		void Perform(ActionExecutionContext context);
	}
}