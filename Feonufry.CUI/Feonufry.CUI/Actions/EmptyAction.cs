
namespace Feonufry.CUI.Actions
{
	public class EmptyAction : IAction
	{
		public string Title
		{
			get { return string.Empty; }
		}

		public bool Available
		{
			get { return true; }
		}

		public void Perform(ActionExecutionContext context)
		{}
	}
}