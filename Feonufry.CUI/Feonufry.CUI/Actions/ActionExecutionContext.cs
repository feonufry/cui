﻿using System.Collections.Generic;

namespace Feonufry.CUI.Actions
{
	public sealed class ActionExecutionContext
	{
		private IActionInput _in;
		private IActionOutput _out;

		private readonly Dictionary<string, object> _map = new Dictionary<string, object>();
		private bool _canceled;
		private bool _skipExceptions = true;
		private bool _actionSucceeded;

		public IActionInput In
		{
			get { return _in; }
		}

		public IActionOutput Out
		{
			get { return _out; }
		}

		public bool ExceptionsAreSkipped
		{
			get { return _skipExceptions; }
		}

		public ActionExecutionContext()
			: this(new ConsoleActionInput(), new ConsoleActionOutput(), true)
		{}

		public ActionExecutionContext(IActionInput @in, IActionOutput @out, bool skipExceptions)
		{
			_in = @in;
			_out = @out;
			_skipExceptions = skipExceptions;
		}

		public void Put<T>(string key, T value)
		{
			_map[key] = value;
		}

		public T Get<T>(string key)
		{
			return (T)_map[key];
		}

		public bool Has(string key)
		{
			return _map.ContainsKey(key);
		}

		public void Cancel()
		{
			_canceled = true;
		}

		public void Continue()
		{
			_canceled = false;
		}

		public bool IsCanceled()
		{
			return _canceled;
		}

		public bool Succeeded()
		{
			return _actionSucceeded;
		}

		public void Success()
		{
			_actionSucceeded = true;
		}

		public void Failure()
		{
			_actionSucceeded = false;
		}

		public void SkipExceptions()
		{
			_skipExceptions = true;
		}

		public void DoNotSkipExceptions()
		{
			_skipExceptions = false;
		}
	}
}