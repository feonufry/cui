﻿
namespace Feonufry.CUI.Actions
{
	public interface IAvailability
	{
		bool Available { get; }
	}
}