﻿using System;

namespace Feonufry.CUI
{
	public class ConsoleActionInput : IActionInput
	{
		public string ReadLine()
		{
			return Console.ReadLine();
		}

		public T ReadEnum<T>()
		{
			var value = ReadLine();
			try
			{
				return (T)Enum.Parse(typeof(T), value);
			}
			catch (ArgumentException ex)
			{
				throw new FormatException(string.Format(
					"Value '{0}' cannot be parsed as enum '{1}' member.", value, typeof(T)), ex);
			}
		}

		public decimal ReadDecimal()
		{
			var value = ReadLine();
			return decimal.Parse(value);
		}

		public int ReadInt32()
		{
			var value = ReadLine();
			return int.Parse(value);
		}

		public int ReadInt32OrDefault()
		{
			var value = ReadLine();
			int result;
			return int.TryParse(value, out result) 
				? result 
				: 0;
		}

		public long ReadInt64()
		{
			var value = ReadLine();
			return long.Parse(value);
		}
	}
}