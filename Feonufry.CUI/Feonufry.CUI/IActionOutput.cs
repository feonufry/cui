﻿using System;

namespace Feonufry.CUI
{
	public interface IActionOutput
	{
		ConsoleColor DefaultColor { get; set; }

		void Write(ConsoleColor color, string text);
		void Write(ConsoleColor color, string format, params object[] args);
		void WriteLine(ConsoleColor color, string text);
		void WriteLine(ConsoleColor color, string format, params object[] args);
		void Write(string text);
		void Write(string format, params object[] args);
		void WriteLine();
		void WriteLine(string text);
		void WriteLine(string format, params object[] args);
	}
}