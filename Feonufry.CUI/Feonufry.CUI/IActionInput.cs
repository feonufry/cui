﻿namespace Feonufry.CUI
{
	public interface IActionInput
	{
		string ReadLine();
		T ReadEnum<T>();
		decimal ReadDecimal();
		int ReadInt32();
		int ReadInt32OrDefault();
		long ReadInt64();
	}
}