﻿using System;
using Feonufry.CUI.Actions;

namespace Feonufry.CUI.Menu.Engine
{
	public abstract class MenuItemBase : IMenuItem
	{
		private string _title;

		public string Title
		{
			get { return _title; }
			set { _title = value; }
		}

		public virtual bool Available
		{
			get { return true; }
		}

		protected MenuItemBase(string title = "")
		{
			_title = title;
		}

		public void Run(ActionExecutionContext context)
		{
			if (!Available)
			{
				throw new InvalidOperationException("Unavailable menu cannot be run.");
			}

			RunInternal(context);
		}

		protected abstract void RunInternal(ActionExecutionContext context);
	}
}
