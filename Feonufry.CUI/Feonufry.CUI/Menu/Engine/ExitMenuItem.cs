﻿
using Feonufry.CUI.Actions;

namespace Feonufry.CUI.Menu.Engine
{
	public class ExitMenuItem : MenuItemBase
	{
		public ExitMenuItem(string title = "") 
			: base(title)
		{}

		protected override void RunInternal(ActionExecutionContext context)
		{
			context.Cancel();
		}
	}
}