﻿
using System;
using Feonufry.CUI.Actions;

namespace Feonufry.CUI.Menu.Engine
{
	public class ActionFactoryMenuItem : MenuItemBase
	{
		private readonly IActionFactory _actionFactory;
		private readonly Type _actionType;

		public ActionFactoryMenuItem(string title, Type actionType, IActionFactory actionFactory) 
			: base(title)
		{
			_actionType = actionType;
			_actionFactory = actionFactory;
		}

		protected override void RunInternal(ActionExecutionContext context)
		{
			var action = _actionFactory.Create(_actionType);
			try
			{
				action.Perform(context);
			}
			finally 
			{
				if (action != null)
				{
					_actionFactory.Release(action);
				}
			}
		}
	}
}
