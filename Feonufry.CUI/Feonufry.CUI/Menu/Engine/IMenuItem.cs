﻿
using Feonufry.CUI.Actions;

namespace Feonufry.CUI.Menu.Engine
{
	public interface IMenuItem
	{
		string Title { get; }
		bool Available { get; }

		void Run(ActionExecutionContext context);
	}
}
