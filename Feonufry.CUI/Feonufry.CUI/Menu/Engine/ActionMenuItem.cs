﻿
using Feonufry.CUI.Actions;

namespace Feonufry.CUI.Menu.Engine
{
	public class ActionMenuItem : MenuItemBase
	{
		private readonly IAction _action;
		private readonly IAvailability _availability;

		public override bool Available
		{
			get { return _availability.Available; }
		}

		public ActionMenuItem(string title, IAction action) 
			: this(title, action, new AlwaysAvailable())
		{}

		public ActionMenuItem(string title, IAction action, IAvailability availability)
			: base(title)
		{
			_action = action;
			_availability = availability;
		}

		protected override void RunInternal(ActionExecutionContext context)
		{
			_action.Perform(context);
		}
	}
}
