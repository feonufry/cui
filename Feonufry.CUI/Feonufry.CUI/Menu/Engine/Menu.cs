﻿using System;
using System.Collections.Generic;
using System.Linq;
using Feonufry.CUI.Actions;

namespace Feonufry.CUI.Menu.Engine
{
	public class Menu : MenuItemBase
	{
		private bool _repeatable;
		private readonly List<IMenuItem> _items = new List<IMenuItem>();
		private string _prompt;
		private string _wrongCommandMessage;
		private string _unavailableCommandMessage;

		public string Prompt
		{
			get { return _prompt; }
			set { _prompt = value; }
		}

		public string WrongCommandMessage
		{
			get { return _wrongCommandMessage; }
			set { _wrongCommandMessage = value; }
		}

		public string UnavailableCommandMessage
		{
			get { return _unavailableCommandMessage; }
			set { _unavailableCommandMessage = value; }
		}

		public bool Repeatable
		{
			get { return _repeatable; }
			set { _repeatable = value; }
		}

		public Menu(string title = "", bool repeatable = true)
			: base(title)
		{
			_repeatable = repeatable;
		}

		public void AddItem(IMenuItem item)
		{
			_items.Add(item);
		}

		public void RemoveItem(IMenuItem item)
		{
			_items.Remove(item);
		}

		protected override void RunInternal(ActionExecutionContext context)
		{
			do
			{
				Print(context);
				ExecuteCommand(ReadCommand(context), context);
			} while (ExecutionIsContinued(context));
			
			context.Continue();
		}

		private bool ExecutionIsContinued(ActionExecutionContext context)
		{
			return !context.Succeeded() 
				|| _repeatable && !context.IsCanceled();
		}

		private void Print(ActionExecutionContext context)
		{
			context.Out.WriteLine();
			context.Out.WriteLine(ConsoleColor.Yellow, Title);

			var key = 1;
			foreach (var item in _items)
			{
				if (item.Available)
				{
					context.Out.Write(ConsoleColor.White, "\t{0}", key);
					context.Out.WriteLine(" - {0}", item.Title);
				}
				else
				{
					context.Out.WriteLine(ConsoleColor.DarkGray, "\t{0} - {1}", key, item.Title);
				}
				++key;
			}			
		}

		private int ReadCommand(ActionExecutionContext context)
		{
			context.Out.WriteLine();
			context.Out.Write("{0} > ", Prompt);

			return context.In.ReadInt32OrDefault();
		}

		private void ExecuteCommand(int key, ActionExecutionContext context)
		{
			context.Failure();
			if (key <= 0 || key > _items.Count())
			{
				context.Out.WriteLine(ConsoleColor.Red, WrongCommandMessage);
				return;
			}

			var command = _items.ElementAt(key - 1);
			if (!command.Available)
			{
				context.Out.WriteLine(ConsoleColor.Red, UnavailableCommandMessage);
				return;
			}

			context.Success();
			command.Run(context);
		}

		public void Run()
		{
			Run(new ActionExecutionContext());
		}

		public void Run(IActionInput @in, IActionOutput @out, bool skipExceptions = true)
		{
			Run(new ActionExecutionContext(@in, @out, skipExceptions));
		}
	}
}
