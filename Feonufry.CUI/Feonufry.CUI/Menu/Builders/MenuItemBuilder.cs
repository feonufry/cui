﻿using System;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Engine;

namespace Feonufry.CUI.Menu.Builders
{
	public sealed class MenuItemBuilder : ILazyBuilder
	{
		private readonly MenuBuilder _parent;
		private readonly Engine.Menu _menu;

		private IAction _action;
		private string _title;
		private IAvailability _availability;

		public MenuItemBuilder(MenuBuilder parent, Engine.Menu menu)
		{
			_parent = parent;
			_menu = menu;
			_title = null;
			_availability = new AlwaysAvailable();
		}

		public void Complete()
		{
			if(_action == null)
			{
				throw new InvalidOperationException("Action was not specified. Specify action during menu item building with 'MenuItemBuilder.Action()' method.");
			}
			if (_availability == null)
			{
				throw new InvalidOperationException("Availability was not specified. Specify availability during menu item building with one of the following methods: 'MenuItemBuilder.AvailableWhen()' or 'MenuItemBuilder.AlwaysAvailable()'.");
			}
			
			_menu.AddItem(new ActionMenuItem(_title, _action, _availability));
		}

		public MenuItemBuilder Title(string title)
		{
			_title = title;
			return this;
		}

		public MenuItemBuilder AlwaysAvailable()
		{
			_availability = new AlwaysAvailable();
			return this;
		}

		public MenuItemBuilder AvailableWhen(Func<bool> availability)
		{
			if (availability == null) throw new ArgumentNullException("availability");

			_availability = new DelegateAvailability(availability);
			return this;
		}

		public MenuItemBuilder AvailableWhen(IAvailability availability)
		{
			if (availability == null) throw new ArgumentNullException("availability");

			_availability = availability;
			return this;
		}

		public MenuItemBuilder Action(Action<ActionExecutionContext> action)
		{
			if (action == null) throw new ArgumentNullException("action");

			_action = new DelegateAction(action);
			return this;
		}

		public MenuBuilder End()
		{
			return _parent;
		}
	}
}