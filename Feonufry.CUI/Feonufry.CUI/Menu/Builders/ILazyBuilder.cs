﻿
namespace Feonufry.CUI.Menu.Builders
{
	public interface ILazyBuilder
	{
		void Complete();
	}
}
