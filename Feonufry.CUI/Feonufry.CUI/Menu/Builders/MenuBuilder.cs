﻿using System;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Engine;
using Feonufry.CUI.Properties;

namespace Feonufry.CUI.Menu.Builders
{
	public class MenuBuilder : CompositeBuilder, ILazyBuilder
	{
		private MenuBuilder _parent;
		private Engine.Menu _menu;
		private IActionFactory _actionFactory = new DefaultActionFactory();
		private bool _actionFactoryWasUsed = false;

		public MenuBuilder()
			: this(null)
		{}

		private MenuBuilder(
			MenuBuilder parent, string prompt = null, 
			string wrongCommandMessage = null, string unavailableCommandMessage = null)
		{
			_parent = parent;
			_menu = new Engine.Menu
			        	{
			        		Prompt = prompt ?? Resources.MenuPrompt,
			        		WrongCommandMessage = wrongCommandMessage ?? Resources.MenuWrongCommandMessage,
			        		UnavailableCommandMessage =
			        			unavailableCommandMessage ?? Resources.MenuUnavailableCommandMessage
			        	};
		}

		public MenuBuilder WithActionFactory(IActionFactory actionFactory)
		{
			if (actionFactory == null) throw new ArgumentNullException("actionFactory");
			if (_actionFactoryWasUsed)
			{
				throw new InvalidOperationException("ActionFactory cannot be changed after Menu Items requiring it were created.");
			}

			_actionFactory = actionFactory;
			return this;
		}

		public MenuBuilder Title(string title)
		{
			_menu.Title = title;
			return this;
		}

		public MenuBuilder Prompt(string prompt)
		{
			_menu.Prompt = prompt;
			return this;
		}

		public MenuBuilder WrongCommandMessage(string message)
		{
			_menu.WrongCommandMessage = message;
			return this;
		}

		public MenuBuilder UnavailableCommandMessage(string message)
		{
			_menu.UnavailableCommandMessage = message;
			return this;
		}

		public MenuBuilder RunnableOnce()
		{
			_menu.Repeatable = false;
			return this;			
		}

		public MenuBuilder Repeatable()
		{
			_menu.Repeatable = true;
			return this;
		}

		public MenuBuilder Exit(string title)
		{
			CompleteCurrentBuilding();
			_menu.AddItem(new ExitMenuItem(title));
			return this;
		}

		public MenuBuilder Item<T>(string title)
			where T : IAction
		{
			CompleteCurrentBuilding();
			_menu.AddItem(new ActionFactoryMenuItem(title, typeof(T), _actionFactory));
			_actionFactoryWasUsed = true;
			return this;
		}

		public MenuBuilder Item(string title, IAction action)
		{
			return Item(title, new AlwaysAvailable(), action);
		}

		public MenuBuilder Item(string title, IAvailability availability, IAction action)
		{
			if (action == null) throw new ArgumentNullException("action");
			if (availability == null) throw new ArgumentNullException("availability");

			CompleteCurrentBuilding();
			_menu.AddItem(new ActionMenuItem(title, action, availability));
			return this;
		}

		public MenuItemBuilder Item()
		{
			CompleteCurrentBuilding();
			
			var builder = new MenuItemBuilder(this, _menu);
			StartBuilding(builder);
			return builder;
		}

		public MenuBuilder Item(string title, Action<ActionExecutionContext> action)
		{
			if (action == null) throw new ArgumentNullException("action");
			return Item(title, new DelegateAction(action));
		}

		public MenuBuilder Item(string title, Func<bool> availability, Action<ActionExecutionContext> action)
		{
			if (action == null) throw new ArgumentNullException("action");
			if (availability == null) throw new ArgumentNullException("availability");

			return Item(title, new DelegateAvailability(availability), new DelegateAction(action));
		}

		public MenuBuilder Submenu(string title = null)
		{
			CompleteCurrentBuilding();

			var builder = new MenuBuilder(this, _menu.Prompt, _menu.WrongCommandMessage, _menu.UnavailableCommandMessage);
			StartBuilding(builder);

			builder.WithActionFactory(_actionFactory);
			if (title != null)
			{
				builder.Title(title);
			}
			return builder;
		}

		public MenuBuilder End()
		{
			if(_parent == null)
			{
				throw new InvalidOperationException("End() operation cannot be used during root menu building. Use GetMenu() to complete building and obtain menu built.");
			}

			CompleteCurrentBuilding();
			_parent.AddSubmenu(_menu, _actionFactoryWasUsed);
			return _parent;
		}

		public Engine.Menu GetMenu()
		{
			if (_parent != null)
			{
				throw new InvalidOperationException("GetMenu() operation cannot be used during submenu building. Use End() to complete submenu construction and return to parent menu building.");
			}
			CompleteCurrentBuilding();
			return _menu;
		}

		private void AddSubmenu(Engine.Menu submenu, bool actionFactoryWasUsed)
		{
			_menu.AddItem(submenu);
			_actionFactoryWasUsed |= actionFactoryWasUsed;
		}

		void ILazyBuilder.Complete()
		{}
	}
}
