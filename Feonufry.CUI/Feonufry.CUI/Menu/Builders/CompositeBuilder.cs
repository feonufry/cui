﻿
namespace Feonufry.CUI.Menu.Builders
{
	public abstract class CompositeBuilder
	{
		private ILazyBuilder _currentBuilder;

		protected void StartBuilding(ILazyBuilder builder)
		{
			_currentBuilder = builder;
		}

		protected void CompleteCurrentBuilding()
		{
			if (_currentBuilder == null) return;

			_currentBuilder.Complete();
			_currentBuilder = null;
		}
	}
}