﻿using System;

namespace Feonufry.CUI
{
	public class ConsoleActionOutput : IActionOutput
	{
		private ConsoleColor _defaultColor;
		private readonly ConsoleColor _outerColor;

		public ConsoleActionOutput()
			: this(Console.ForegroundColor)
		{}

		public ConsoleActionOutput(ConsoleColor outerColor)
		{
			_outerColor = outerColor;
			DefaultColor = ConsoleColor.Gray;
		}

		public ConsoleColor DefaultColor
		{
			get { return _defaultColor; }
			set 
			{ 
				_defaultColor = value;
				Console.ForegroundColor = _defaultColor;
			}
		}

        public void Write(ConsoleColor color, string text)
        {
            Console.ForegroundColor = color;
			Console.Write(text);
            Console.ForegroundColor = _outerColor;
        }

        public void Write(ConsoleColor color, string format, params object[] args)
        {
            Console.ForegroundColor = color;
			Console.Write(format, args);
			Console.ForegroundColor = _outerColor;
        }

        public void WriteLine(ConsoleColor color, string text)
        {
            Console.ForegroundColor = color;
			Console.WriteLine(text);
			Console.ForegroundColor = _outerColor;
        }

        public void WriteLine(ConsoleColor color, string format, params object[] args)
        {
            Console.ForegroundColor = color;
			Console.WriteLine(format, args);
			Console.ForegroundColor = _outerColor;
        }

		public void WriteLine()
		{
			Console.WriteLine();
		}

		public void Write(string text)
        {
			Write(DefaultColor, text);
        }

        public void Write(string format, params object[] args)
        {
            Write(DefaultColor, format, args);
        }

        public void WriteLine(string text)
        {
            WriteLine(DefaultColor, text);
        }

        public void WriteLine(string format, params object[] args)
        {
			WriteLine(DefaultColor, format, args);
        }
    }
}