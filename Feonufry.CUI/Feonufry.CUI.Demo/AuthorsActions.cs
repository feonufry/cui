﻿using System;
using System.Collections.Generic;
using System.Linq;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;

namespace Feonufry.CUI.Demo
{
    public class AuthorsActions
    {
        private List<Author> _authors = new List<Author>
            {
                new Author("Evans E.", "Eric Evans"),
                new Author("Fowler M.", "Martin Fowler"),
                new Author("Gamma E.", "Erich Gamma")
            };

        public void ViewAuthors(ActionExecutionContext context)
        {
            context.Out.WriteLine("Зарегистрированные авторы:");
            foreach (var author in _authors)
            {
                context.Out.WriteLine(" - {0} ({1})", author.ShortName, author.FullName);
            }
        }

        public void CreateAuthor(ActionExecutionContext context)
        {
            context.Out.Write("Полное имя: ");
            var fullName = context.In.ReadLine();

            context.Out.Write("Краткое имя: ");
            var shortName = context.In.ReadLine();

            _authors.Add(new Author(shortName, fullName));
            context.Out.WriteLine(ConsoleColor.Green, "Автор добавлен.");
        }

        public bool AuthorsUpdateAvailable()
        {
            return _authors.Count() > 0;
        }

        public void SelectAuthorToUpdate(ActionExecutionContext context)
        {
            var submenu = new MenuBuilder()
                .Title("Зарегистрированные авторы:")
                .Prompt("Укажите автора:")
                .RunnableOnce();
            foreach (var author in _authors)
            {
                var authorId = author.Id;
                submenu.Item()
                       .AlwaysAvailable()
                       .Title(string.Format("{0} ({1})", author.FullName, author.ShortName))
                       .Action(ctx => UpdateAuthor(ctx, authorId));
            }
            submenu
                .Exit("Отмена")
                .GetMenu().Run();
        }

        public void UpdateAuthor(ActionExecutionContext context, Guid id)
        {
            context.Out.Write("Новое полное имя: ");
            var fullName = context.In.ReadLine();

            context.Out.Write("Новое краткое имя: ");
            var shortName = context.In.ReadLine();

            var author = _authors.Where(a => a.Id == id).Single();
            author.FullName = fullName;
            author.ShortName = shortName;
            context.Out.WriteLine(ConsoleColor.Green, "Автор был изменен.");
        }
    }
}