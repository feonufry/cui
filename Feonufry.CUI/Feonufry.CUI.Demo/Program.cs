﻿
using Feonufry.CUI.Menu.Builders;

namespace Feonufry.CUI.Demo
{
	class Program
	{
		private static void Main(string[] args)
		{
			var authorsActions = new AuthorsActions();
			new MenuBuilder()
				.Title("Демонстрация Меню")
				//.Prompt("Enter command:")
				//.WrongCommandMessage("Wrong command. Try again.")
				.Repeatable()
				.Submenu()
					.Title("Управление авторами")
					.Item("Просмотр авторов", authorsActions.ViewAuthors)
					.Item("Добавление автора", authorsActions.CreateAuthor)
					.Item("Изменение автора", authorsActions.AuthorsUpdateAvailable, authorsActions.SelectAuthorToUpdate)
					.Exit("Назад")
				.End()
				.Exit("Выход")
				.GetMenu().Run();
		}
	}
}
