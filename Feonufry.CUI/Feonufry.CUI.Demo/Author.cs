﻿using System;

namespace Feonufry.CUI.Demo
{
    public class Author
    {
        public Guid Id { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }

        public Author(string shortName, string fullName)
        {
            Id = Guid.NewGuid();
            ShortName = shortName;
            FullName = fullName;
        }
    }
}